﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EntityFrameworkDatabaseFirst.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeContext db = new EmployeeContext();
      
        public ActionResult Index()
        {
            return View(db.Employee.ToList());
        }
        [HttpGet]
        public ActionResult Creat()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Creat(Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employee.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}