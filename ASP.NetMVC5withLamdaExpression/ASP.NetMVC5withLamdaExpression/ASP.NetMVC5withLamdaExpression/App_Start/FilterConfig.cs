﻿using System.Web;
using System.Web.Mvc;

namespace ASP.NetMVC5withLamdaExpression
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
